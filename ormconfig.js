const rootDir = process.env.NODE_ENV === "development" ? "src" : "build";
// const rootDir = "build"
//process.env.NODE_ENV="test"
console.log("what is node env:", process.env.NODE_ENV)

module.exports = {
    "type": "mongodb",
    "url": "mongodb+srv://aaa:bbb@cluster0-yachn.mongodb.net/OMMS?retryWrites=true&w=majority",
    "useNewUrlParser": true,
    "synchronize": true,
    "logging": false,
    "entities": [rootDir + "/entity/*.{js,ts}"],
    "migrations": [rootDir + "/migration/*.{js,ts}"],
    "subscribers": [rootDir + "/subscriber/*.{js,ts}"],
    "cli": {
        "entitiesDir": rootDir + "/entity",
        "migrationsDir": rootDir + "/migration",
        "subscribersDir": rootDir + "/subscriber"
    }
}
