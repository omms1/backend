
export enum Role {
    Admin = 'Admin',
    SuperAdmin = 'SuperAdmin',
    User = 'User',
    // Auser = 'Auser', //
    // Buser = 'Buser',
    // Cuser = 'Cuser'
}

export enum ERRCODE {
    E_OK = 200,
    E_201 = 201,
    E_204 = 204,
    E_400 = 400,
    E_401 = 401,
    E_404 = 404,
    E_409 = 409
}

export enum ERRSTR {
    S_OK = '',
    S_NOTFOUND = 'Cannot find user',
    S_SAVEERR = 'Save user error',
    S_BADPARAMETER = 'bad parameters',
    S_BADPASSWORD = 'username and password combination error',
    S_SAVEERROR = 'writing error'
}

export class MessageError {
    data: any
    code: number
    msg: string
    constructor(code: number = ERRCODE.E_OK, msg: string = null, data: any = null) {
        this.data = data
        this.code= code
        this.msg = msg
    }
}
