import {Router} from "express";
import {OrderController} from "../controller/OrderController";
import {checkJwt} from "../middlewares/checkJwt";
import {checkRole} from "../middlewares/checkRole";

const router = Router()

router.get('/', [checkJwt], OrderController.listAll)//post pageno orderid

router.get('/listByCondition', [checkJwt], OrderController.listByCondition) //post pageno orderid

router.get('/:orderid',[checkJwt, checkRole(['admin'])], OrderController.getOneById)

router.post('/',[checkJwt, checkRole(['admin'])], OrderController.newOrder)

router.patch('/:orderid', [checkJwt], OrderController.editOrder)

router.delete('/:orderid', [checkJwt, checkRole(['admin'])], OrderController.deleteOrder)
export default router
