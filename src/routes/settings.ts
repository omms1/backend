import {Router} from "express";
import {checkJwt} from "../middlewares/checkJwt";
import {checkRole} from "../middlewares/checkRole";
import {SettingsController} from "../controller/SettingsController";

const router = Router()

router.get('/whitelist', [checkJwt, checkRole(['admin'])], SettingsController.listWhitelist)

// router.get('/:orderid', ToASubOrderController.getOneById)
//
router.post('/whitelist', [checkJwt, checkRole(['admin'])], SettingsController.newWhitelist)
//
//router.patch('/whitelist/:id', [checkJwt, checkRole(['admin'])] ,SettingsController.editToASubOrder)
//
router.delete('/whitelist/:id', [checkJwt, checkRole(['admin'])], SettingsController.deleteWhitelist)
export default router
