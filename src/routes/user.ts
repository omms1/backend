import {Router} from "express";
import {UserController} from "../controller/UserController";
import {checkJwt} from "../middlewares/checkJwt";
import {checkRole} from "../middlewares/checkRole";

const regxUserId = '[a-z0-9]+'
const router = Router()

router.get(`/:userid(${regxUserId})`,[checkJwt], UserController.getOneById);

router.get('/',[checkJwt], UserController.listAll)

router.post('/', [checkJwt, checkRole(['admin'])], UserController.newUser);

router.get('/checkUser/:userName', UserController.checkuserNameExisted)

router.get('/checkUser/email/:useremail', UserController.checkuseremailExisted)

router.put(`/:userid(${regxUserId})`, [checkJwt, checkRole(['admin'])], UserController.editUser)

router.delete(`/:userid(${regxUserId})`, [checkJwt, checkRole(['admin'])], UserController.deleteUser);



// @ts-ignore
export default router
