import {Router} from "express";
import xlsx from "./xlsx"
import user from './user'
import auth from './auth'
import order from './order'
import factory from './factory'
import toASubOrder from './toASubOrder'
import sendEmail from './sendEmail'
import settings from "./settings";
const routes = Router()

routes.use('/user', user);

routes.use('/order', order);

routes.use('/auth', auth);

routes.use('/xlsx', xlsx);

routes.use('/factory', factory);

routes.use('/toASubOrder', toASubOrder);

routes.use('/sendEmail', sendEmail)

routes.use('/settings', settings)
export default routes
