import {Router} from "express";
import {checkJwt} from "../middlewares/checkJwt";
import {checkRole} from "../middlewares/checkRole";
import {FactoryController} from "../controller/FactoryController";

const router = Router()

router.get('/',[checkJwt], FactoryController.listAll)

router.get('/listAllCustomers', [checkJwt, checkRole(['admin'])], FactoryController.listAllCustomers)

router.get('/listCustomersNotInWL', [checkJwt, checkRole(['admin'])], FactoryController.listCustomersNotInWL)

router.get('/:factoryid',[checkJwt], FactoryController.getOneById)

router.post('/', [checkJwt, checkRole(['admin'])], FactoryController.newOrder)

router.put('/:factoryid', [checkJwt, checkRole(['admin'])] , FactoryController.editOrder)

router.delete('/:factoryid', [checkJwt, checkRole(['admin'])], FactoryController.deleteOrder)

router.get('/getname/:factoryname',[checkJwt], FactoryController.getOneByName)



export default router
