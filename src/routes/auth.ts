import {Router} from "express";
import {AuthController} from "../controller/AuthController";
import {UserController} from "../controller/UserController";

const router = Router()

//login

router.use('/login', AuthController.login)

router.post('/register',UserController.newUser)
//change password

router.use('/change-password', AuthController.changePassword)

export default router
