import {Router} from "express";
import {OrderStatusLogController} from "../controller/OrderStatusLogController";


const router = Router()

router.post('/', OrderStatusLogController.getOrderInfosForSendingEmail)


export default router
