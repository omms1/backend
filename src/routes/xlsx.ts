import {Router} from "express";
import {xlsxController} from "../controller/xlsxController";
import {upload} from "../middlewares/upload";
 const multer = require('multer');


// let upload = multer({storage: storage});

const router = Router();
router.get('/download', xlsxController.exportSheet);
router.post('/upload',  multer(upload).any(), xlsxController.imageUpload);
export default router;
