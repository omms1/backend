import {Router} from "express";
import {checkJwt} from "../middlewares/checkJwt";
import {checkRole} from "../middlewares/checkRole";
import {ToASubOrderController} from "../controller/ToASubOrderController";

const router = Router()

router.get('/',[checkJwt], ToASubOrderController.listAll)

router.get('/:orderid',[checkJwt], ToASubOrderController.getOneById)

router.post('/', [checkJwt, checkRole(['admin'])], ToASubOrderController.newToASubOrder)

router.put('/:orderid', [checkJwt], ToASubOrderController.editToASubOrder)

router.delete('/:orderid', [checkJwt, checkRole(['admin'])], ToASubOrderController.deleteOrder)
export default router
