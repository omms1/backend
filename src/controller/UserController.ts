import {Request, Response} from "express";
import {getRepository} from "typeorm";
import {User} from "../entity/User";
import {validate} from "class-validator";
import {ERRCODE, ERRSTR, MessageError, Role} from "../helper/helper";
import {Factory, FactoryType} from "../entity/Factory";
import {FactoryController} from "./FactoryController";

const columnFilter: any = {
    select: ['id', 'username', 'password', 'role']
}

export class UserController {
    public static get repo() {
        return getRepository(User)
    }

    static listAll = async (req: Request, res: Response) => {
        let id = JSON.parse(<string>req.headers['userinfo']).user.id
        let user:User = null
        let users:User[] = []
        let userCheck:User = null
        try{
            user = await UserController.repo.findOneOrFail(id)
        }catch (e) {
           return res.status(ERRCODE.E_404).send(new MessageError(ERRCODE.E_404,ERRSTR.S_NOTFOUND))
        }
        try {
            if (user.role.some(x => x === Role.User)) {
                userCheck = await UserController.repo.findOneOrFail(user.id)
                users.push(userCheck)
            } else {
                users = await UserController.repo.find()
            }
        } catch (e) {
            return res.status(ERRCODE.E_404).send(new MessageError(ERRCODE.E_404,ERRSTR.S_NOTFOUND))
        }
        return res.status(ERRCODE.E_201).send(new MessageError(ERRCODE.E_201,'', users))
    }

    static checkuserNameExisted = async (req:Request, res:Response) => {
        const userName: string = req.params.userName
        console.log('kankan name',userName)
        const userRepo = UserController.repo
        const numb = await userRepo.count({userName})
        console.log('numb name ===>',numb)
        return res.send(new MessageError(ERRCODE.E_OK, '', numb))
    }

    static checkuseremailExisted = async (req:Request, res:Response) => {
        const useremail: string = req.params.useremail
        const userRepo = UserController.repo
        const numb = await userRepo.count({useremail})
        return res.send(new MessageError(ERRCODE.E_OK, '', numb))
    }


    static getOneById = async (req: Request, res: Response) => {

        const id = req.params.userid
        try {
            const user = await UserController.repo.findOneOrFail(id)
            return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, '', user))
        }catch (e) {
            return res.status(ERRCODE.E_400).send(new MessageError(ERRCODE.E_400, ERRSTR.S_NOTFOUND))
        }
    }

    static newUser = async (req: Request, res: Response) => {
        const {userName, userPicture, password, phone1, phone2, useremail, role, companyType, companyId, duty, createdAt, updateAt,
        deleteAt, active} = req.body
        let user = new User(userName, userPicture, password, phone1, phone2, useremail, role, companyType, companyId, duty, createdAt,
            updateAt, deleteAt, active)
        console.log('new++>' , user)
        let company: Factory = null
        try {
            const error = await validate(user)
            if (error.length > 0) {
                console.log('user length ->',user)
                return res.status(ERRCODE.E_401).send(new MessageError(ERRCODE.E_401,'', error))
            }
            user.hashPassword()
            await UserController.repo.save(user)
        }catch (e) {
            return res.status(ERRCODE.E_400).send(new MessageError(ERRCODE.E_400, `${ERRSTR.S_SAVEERROR} - ${e.errmsg}`))
        }
        try {
            company = await FactoryController.repo.findOneOrFail(user.companyId)
        } catch (e) {
            return res.status(ERRCODE.E_404).send(new MessageError(ERRCODE.E_404, ERRSTR.S_NOTFOUND, e.message))
        }
        company.contact = user.userName
        try {
            await FactoryController.repo.save(company)
        }catch (e) {
            return res.status(ERRCODE.E_401).send(new MessageError(ERRCODE.E_401, ERRSTR.S_SAVEERROR, e.message))
        }
        return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, '', user))
    }


    static editUser = async (req: Request, res: Response) => {
        const id = req.params.userid
        const {userName, userPicture, password, phone1, phone2, useremail, role, companyType, companyId, duty, createdAt, updateAt,
            deleteAt, active} = req.body
        let userOld: User = null;
        let company: Factory = null
        try {
            userOld = await UserController.repo.findOneOrFail(id)
        }catch (e) {
            return res.status(ERRCODE.E_400).send('Cannot find user')
        }
        let user = new User(userName, userPicture, password, phone1, phone2, useremail, role, companyType, companyId, duty, createdAt,
            updateAt, deleteAt, active)
        user.id = userOld.id
        try {
            await UserController.repo.save(user)
        }catch (e) {
            return res.status(ERRCODE.E_401).send(new MessageError(ERRCODE.E_401, ERRSTR.S_SAVEERR))
        }
        try {
            company = await FactoryController.repo.findOneOrFail(user.companyId)
        } catch (e) {
            return res.status(ERRCODE.E_404).send(new MessageError(ERRCODE.E_404, ERRSTR.S_NOTFOUND, e.message))
        }
        company.contact = user.userName
        try {
            await FactoryController.repo.save(company)
        }catch (e) {
            return res.status(ERRCODE.E_401).send(new MessageError(ERRCODE.E_401, ERRSTR.S_SAVEERROR, e.message))
        }
        return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, '', user))
    }

    static deleteUser = async (req: Request, res: Response) => {
        const id = req.params.userid
        try {
            await UserController.repo.delete(id)
        }catch (e) {
            return res.status(ERRCODE.E_401).send(new MessageError(ERRCODE.E_401, ''))
        }
        return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, ERRSTR.S_OK))
    }
}
