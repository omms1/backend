import {getRepository} from "typeorm";
import {Request, Response} from "express";
import {ERRCODE, ERRSTR, MessageError} from "../helper/helper";
import {validate} from "class-validator";
import {ToASubOrder} from "../entity/ToASubOrder";
import {User} from "../entity/User";
import {UserController} from "./UserController";
import {Factory, FactoryType} from "../entity/Factory";
import {Order} from "../entity/Order";
import {OrderController} from "./OrderController";
import {FactoryController} from "./FactoryController";

export class ToASubOrderController {
    public static get repo() {
        return getRepository(ToASubOrder)
    }

    static listAll = async (req: Request, res: Response) => {
        let id = JSON.parse(<string>req.headers['userinfo']).user.id
        let user: User = null
        let factory: Factory = null
        let toASubOrders: ToASubOrder[] = []
        let toASubOrderArr = null
        try {
            user = await UserController.repo.findOneOrFail(id)
        } catch (e) {
            return res.status(ERRCODE.E_404).send(new MessageError(ERRCODE.E_404, ERRSTR.S_NOTFOUND))
        }
        if (user.companyType === FactoryType.amanufacturer) {
            try {
                factory = await FactoryController.repo.findOneOrFail(user.companyId)
            } catch (e) {
                return res.status(ERRCODE.E_404).send(new MessageError(ERRCODE.E_404, ERRSTR.S_NOTFOUND))
            }
            for (let i = 0; i < factory.toASubOrderIds.length; i++) {
                try {
                    toASubOrderArr = await ToASubOrderController.repo.findOneOrFail(factory.toASubOrderIds[i])
                } catch (e) {
                    return res.status(ERRCODE.E_404).send(new MessageError(ERRCODE.E_404, ERRSTR.S_NOTFOUND))
                }
                toASubOrders.push(toASubOrderArr)
            }
        } else if (user.companyType === FactoryType.bcompany) {
            try {
                toASubOrders = await ToASubOrderController.repo.find()
            } catch(e) {
                return res.status(ERRCODE.E_404).send(new MessageError(ERRCODE.E_404, ERRSTR.S_NOTFOUND))
            }
        }
        return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, ERRSTR.S_OK, toASubOrders))
    }

    static getOneById = async (req: Request, res: Response) => {
        const id = req.params.orderid
        console.log('get one by id->', id)
        try{
            const user = await ToASubOrderController.repo.findOneOrFail(id)
            return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, ERRSTR.S_OK, user))
        }catch (e) {
            return res.status(ERRCODE.E_401).send(ERRSTR.S_NOTFOUND)
        }
    }

    static newToASubOrder = async (req: Request, res: Response) => {
        let {contractNum, orderId, contractPriceRmb, downPaymentRmb, note, flightInformation, part, delivery} = req.body
        let user: ToASubOrder = new ToASubOrder(contractNum, orderId, contractPriceRmb, downPaymentRmb,
            note, flightInformation, part, delivery)
        try{
            const errors = await validate(user)
            if (errors.length > 0) {
                return res.status(ERRCODE.E_400).send(new MessageError(ERRCODE.E_400, ERRSTR.S_NOTFOUND))
            }
            await ToASubOrderController.repo.save(user)
        }catch (e) {
            return res.status(ERRCODE.E_404).send(new MessageError(ERRCODE.E_404, ERRSTR.S_SAVEERR))
        }
        return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, ERRSTR.S_OK, user))
    }

    static editToASubOrder = async (req: Request, res: Response) => {
        let id = req.params.orderid
        let {contractNum, orderId, contractPriceRmb, downPaymentRmb, note, flightInformation, part, delivery} = req.body
        let userOld: ToASubOrder = null
        try {
            userOld = await ToASubOrderController.repo.findOneOrFail(id)
        }catch (e) {
            return res.status(ERRCODE.E_404).send(new MessageError(ERRCODE.E_404, ERRSTR.S_NOTFOUND))
        }
        let user = new ToASubOrder(contractNum, orderId, contractPriceRmb, downPaymentRmb, note,
            flightInformation, part, delivery)
        user.id = userOld.id
        const errors = await validate(user)
        if (errors.length > 0) {
            return res.status(ERRCODE.E_400).send(new MessageError(ERRCODE.E_400,'',errors))
        }
        try {
            await ToASubOrderController.repo.save(user)
        }catch (e) {
            return res.status(ERRCODE.E_400).send(new MessageError(ERRCODE.E_400, ERRSTR.S_SAVEERR))
        }
        return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, ERRSTR.S_OK, user))
    }

    static deleteOrder = async (req: Request, res: Response) => {
        const id = req.params.orderid
        try{
            await ToASubOrderController.repo.findOneOrFail(id)
        }catch (e) {
            return res.status(ERRCODE.E_404).send(e)
        }
        await ToASubOrderController.repo.delete(id)
        return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, ERRSTR.S_OK))
    }
}
