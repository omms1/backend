import {Request, Response} from "express";
import {getRepository} from "typeorm";
import {User} from "../entity/User";
import {ERRCODE, ERRSTR, MessageError} from "../helper/helper";
import * as jwt from "jsonwebtoken"
import config from "../config/config"

export class AuthController {
    private static get repo() {
        return getRepository(User)
    }
    static login = async (req: Request, res: Response) => {
        console.log('login->',req.body)
        let {useremail, password} = req.body

        if (!(useremail && password)) {
            console.log('nana-->',req.body)
            return res.status(ERRCODE.E_404).send(new MessageError(ERRCODE.E_404, ERRSTR.S_BADPARAMETER))
        }
        let user: User = null
        try {
            user = await AuthController.repo.findOneOrFail({where:{useremail}})
            console.log('user1->',user)
        }catch (e) {

            console.log('nihao==>', e)
            return res.status(ERRCODE.E_404).send(new MessageError(ERRCODE.E_404, ERRSTR.S_NOTFOUND))
        }
        if (!user.checkIfUnencryptPasswordValid(password)) {
            console.log('yyyy-->',req.body)
            return res.status(ERRCODE.E_404).send(new MessageError(ERRCODE.E_404, ERRSTR.S_BADPASSWORD))

        }
        const token = jwt.sign({userId: user.id, useremail, role: user.role},
            config.jwtSecret,
            {'expiresIn': '1h'}
            )
        return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, ERRSTR.S_OK, {token, user}))
    }

    static changePassword = async (req: Request, res: Response) => {
    }

}
