import {Request, Response} from "express";
import {getRepository} from "typeorm";
import {validate} from "class-validator";
import {ERRCODE, ERRSTR, MessageError} from "../helper/helper";
import {MsgType, Whitelist} from "../entity/Whitelist";
import {User} from "../entity/User";

// const columnFilter: any = {
//     select: ['id', 'username', 'password', 'role']
// }

export class SettingsController {
    private static get whitelistRepo() {
        return getRepository(Whitelist)
    }

    static listWhitelist = async (req: Request, res: Response) => {
        const whitelists = await SettingsController.whitelistRepo.find()
        return res.status(ERRCODE.E_201).send(new MessageError(ERRCODE.E_OK,'', whitelists))
    }

    /*static getOneById = async (req: Request, res: Response) => {
        const id = req.params.userid
        try {
            const user = await UserController.repo.findOneOrFail(id)
            return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, '', user))
        }catch (e) {
            return res.status(ERRCODE.E_400).send(new MessageError(ERRCODE.E_400, ERRSTR.S_NOTFOUND))
        }
    }*/

    static newWhitelist = async (req: Request, res: Response) => {
        const {companyId, messageType} = req.body
        console.log('req.body-->',req.body);
        /*const reqObj = JSON.parse(req.body)
        const {companyId} = reqObj;
        console.log('reqObj-->',reqObj);*/
        let id = res.locals.jwtPayload.userId
        let operator: User = null
        let whitelist: Whitelist;

        try {
            operator = await getRepository(User).findOneOrFail(id)
            whitelist = new Whitelist(companyId, parseInt(messageType, 10)===0? MsgType.msg: MsgType.email, operator.userName)
            const errors = await validate(whitelist)
            if (errors.length > 0) {
                return res.status(ERRCODE.E_400).send(new MessageError(ERRCODE.E_400, errors.toString()))
            }

            await SettingsController.whitelistRepo.save(whitelist)
        }catch (e) {
            return res.status(ERRCODE.E_400).send('Catch error failed' + e)
        }
        return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, '', whitelist))
    }


    // static editUser = async (req: Request, res: Response) => {
    //     const id = req.params.userid
    //     let {useremail, role} = req.body
    //     let user: User = null
    //     try {
    //         user = await UserController.repo.findOneOrFail(id)
    //     }catch (e) {
    //         return res.status(ERRCODE.E_400).send('Cannot find user')
    //     }
    //     user.useremail = useremail
    //     user.role = role
    //     try {
    //         await UserController.repo.save(user)
    //     }catch (e) {
    //         return res.status(ERRCODE.E_401).send(new MessageError(ERRCODE.E_401, ERRSTR.S_SAVEERR))
    //     }
    //     return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, '', user))
    // }

    static deleteWhitelist = async (req: Request, res: Response) => {
        const id = req.params.id
        console.log('id--->', id);
        try {
            await SettingsController.whitelistRepo.delete(id)
        }catch (e) {
            return res.status(ERRCODE.E_401).send(new MessageError(ERRCODE.E_401, ''))
        }
        return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, ERRSTR.S_OK))
    }
}
