import {getRepository} from "typeorm";
import {Request, Response} from "express";
import {ERRCODE, ERRSTR, MessageError} from "../helper/helper";
import {Order, OrderStatus} from "../entity/Order";
import {validate} from "class-validator";
import {OrderStatusLogService} from "../service/OrderStatusLogService";
import {User} from "../entity/User";
import {UserController} from "./UserController";
import {Factory, FactoryType} from "../entity/Factory";
import {ToASubOrder} from "../entity/ToASubOrder";
import {ToASubOrderController} from "./ToASubOrderController";

interface ExOrderInfo {
    order: Order
    cFactory: Factory
    aFactorys: Factory[]
    subOrders: ToASubOrder[]
}

export class OrderController {
    public static get repo() {
        return getRepository(Order)
    }


    static listAll = async (req: Request, res: Response) => {
        let id = JSON.parse(<string>req.headers['userinfo']).user.id
        let user: User = null
        let orders: Order[] = []
        let factoryDeleteObjectKey = ['factoryId', 'factoryAdvancedPayment', 'factoryFinalPayment']
        try {
            user = await UserController.repo.findOneOrFail(id)
        } catch (e) {
            return res.status(ERRCODE.E_401).send(new MessageError(ERRCODE.E_401, ERRSTR.S_NOTFOUND))
        }
        try {
            let order: Order = null
            if (user.companyType === FactoryType.customer) {
                order = await OrderController.repo.findOneOrFail({where: {customerId: user.companyId}})
                    for (let i = 0; i < factoryDeleteObjectKey.length; i++) {
                        delete order[factoryDeleteObjectKey[i]]
                }
                orders.push(order)
                console.log(orders)
            } else if (user.companyType === FactoryType.bcompany) {
                orders = await OrderController.repo.find()
            }
        } catch (e) {
            return res.status(ERRCODE.E_404).send(new MessageError(ERRCODE.E_404, ERRSTR.S_NOTFOUND))
        }
        return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, ERRSTR.S_OK, orders))
    }

    static listByCondition = async (req: Request, res: Response) => {
        console.log(req.body)
       // console.log('userinfo--->',req.headers['userinfo']);
       // let id = JSON.parse(<string>req.headers['userinfo']).user.id
        let id = res.locals.jwtPayload.userId
        let user: User = null
        let orders: Order[] = []
        let amount: number = 0
            let factoryDeleteObjectKey = ['factoryId', 'factoryAdvancedPayment', 'factoryFinalPayment']

        const {pageNo, pageSize, orderId} = req.query;
        console.log('pageNo=', pageNo, ' pageSize=', pageSize);
        /*try {
            user = await UserController.repo.findOneOrFail(id)
        } catch (e) {
            return res.status(ERRCODE.E_401).send(new MessageError(ERRCODE.E_401, ERRSTR.S_NOTFOUND))
        }*/
        try {
            /*if (user.companyType === FactoryType.amanufacturer) {
                [orders, amount] = await OrderController.repo.findAndCount({
                    where: {
                        factoryId: user.companyId
                    },
                    skip: pageNo * pageSize,
                    take: pageSize,
                });
                console.log('amount is ', amount);
            } else if (user.companyType === FactoryType.customer) {
                [orders, amount] = await OrderController.repo.findAndCount({where:
                        {
                            customerId: user.companyId
                        },
                    skip: pageNo * pageSize,
                    take: pageSize,
                });
                console.log('amount is ', amount);
            } else {*/
            // orders = await OrderController.repo.find({
            //     skip: pageNo * pageSize,
            //     take: pageSize,
            // });
              const [orders, amout] = await OrderController.repo.createQueryBuilder('order').limit(pageSize).offset(pageNo * pageSize).getManyAndCount()
                console.log('orders are ', orders);
                console.log('amout are ', amout);
            //}
        } catch (e) {
            console.log('e', e);
            return res.status(ERRCODE.E_404).send(new MessageError(ERRCODE.E_404, ERRSTR.S_NOTFOUND))
        }

//        console.log('exorders -> ', orders)
        return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, ERRSTR.S_OK, orders))
    }

    // 1, B company, admin, can see all data
    // 2, B company, common user,
    // 3, C company, common user
    // 4, A company, common user
    // localhost:3838/order?startTime=1091&endTime=2008&status='inpurograss'

    static getOneById = async (req: Request, res: Response) => {
        const params = req.params.orderid
        try {
            const user = await OrderController.repo.findOneOrFail(params)
            return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, ERRSTR.S_OK, user))
        } catch (e) {
            return res.status(ERRCODE.E_401).send(ERRSTR.S_NOTFOUND)
        }
    };

    static newOrder = async (req: Request, res: Response) => {
        let userId = JSON.parse(<string>req.headers['userinfo']).user.id
        let {
            note, status, invoiceNumber, customerId, factoryId, poNumber, balanceUsd, balanceCad, flightInformation,
            cabinetType, toASubOrderIds, expectedDeliveryDate, factoryAdvancedPayment, factoryFinalPayment
        } = req.body
        let order: Order = new Order(note, status, invoiceNumber, customerId, factoryId, poNumber, balanceUsd, balanceCad,
            flightInformation, cabinetType, toASubOrderIds, expectedDeliveryDate, factoryAdvancedPayment,
            factoryFinalPayment)
        let id = res.locals.jwtPayload.userId
        let toASubOrders: ToASubOrder[] = []
        let operator: User = null
        try {
            operator = await UserController.repo.findOneOrFail(userId)
        } catch (e) {
            return res.status(ERRCODE.E_401).send(new MessageError(ERRCODE.E_401, ERRSTR.S_NOTFOUND))
        }
        try {
            const errors = await validate(order)
            operator = await getRepository(User).findOneOrFail(id)
            if (errors.length > 0) {
                return res.status(ERRCODE.E_400).send(new MessageError(ERRCODE.E_400, ERRSTR.S_NOTFOUND))
            }
            await OrderController.repo.save(order)
        } catch (e) {
            return res.status(ERRCODE.E_404).send(new MessageError(ERRCODE.E_404, ERRSTR.S_SAVEERR))
        }
        let orderStatusLogService: OrderStatusLogService = new OrderStatusLogService();
        const orderId = order.id.toString();
        const result = await orderStatusLogService.createOrderStatusLog(order.id.toString(), OrderStatus.initial, order.status, new Date(), operator.userName);
        console.log(`change status of orderid ${orderId},from ${OrderStatus.initial} to ${order.status}, result is ${result}`);
        for (let i = 0; i < order.toASubOrderIds.length; i++) {
            try {
                toASubOrders[i] = await ToASubOrderController.repo.findOneOrFail(order.toASubOrderIds[i])
            }catch (e) {
                return res.status(ERRCODE.E_401).send(new MessageError(ERRCODE.E_401, ERRSTR.S_NOTFOUND,e.message))
            }
            toASubOrders[i].orderId = order.id.toString()
            await ToASubOrderController.repo.save(toASubOrders)
        }
        return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, ERRSTR.S_OK, order))
    }

    static editOrder = async (req: Request, res: Response) => {
        let userId = JSON.parse(<string>req.headers['userinfo']).user.id
        let operator: User = null
        let id = req.params.orderid
        try {
            operator = await UserController.repo.findOneOrFail(userId)
        } catch (e) {
            return res.status(ERRCODE.E_401).send(new MessageError(ERRCODE.E_401, ERRSTR.S_NOTFOUND))
        }
        let {
            note, status, invoiceNumber, customerId, factoryId, poNumber, balanceUsd, balanceCad, flightInformation,
            cabinetType, toASubOrderIds, expectedDeliveryDate, factoryAdvancedPayment, factoryFinalPayment
        } = req.body
        let orderOld: Order = null
        let toASubOrders: ToASubOrder[] = []
        let newToASubOrder: ToASubOrder[] = []
        try {
            orderOld = await OrderController.repo.findOneOrFail(id)
        } catch (e) {
            return res.status(ERRCODE.E_404).send(new MessageError(ERRCODE.E_404, ERRSTR.S_NOTFOUND))
        }
        let order = new Order(note, status, invoiceNumber, customerId, factoryId, poNumber, balanceUsd, balanceCad,
            flightInformation, cabinetType, toASubOrderIds, expectedDeliveryDate, factoryAdvancedPayment,
            factoryFinalPayment)

        if (orderOld.status !== order.status) {
            let orderStatusLogService: OrderStatusLogService = new OrderStatusLogService();
            const result = orderStatusLogService.createOrderStatusLog(id.toString(), orderOld.status, order.status, new Date(), operator.userName);
            console.log(`change status of orderid ${id}, result is ${result}`)
        }
        order.id = orderOld.id
        const errors = await validate(order)
        if (errors.length > 0) {
            return res.status(ERRCODE.E_400).send(new MessageError(ERRCODE.E_400, '', errors))
        }
        try {
            console.log(order)
            await OrderController.repo.save(order)
        } catch (e) {
            return res.status(ERRCODE.E_400).send(new MessageError(ERRCODE.E_400, ERRSTR.S_SAVEERR))
        }
        for (let i = 0; i< order.toASubOrderIds.length; i++) {
            try {
                toASubOrders[i] = await ToASubOrderController.repo.findOneOrFail(order.toASubOrderIds[i])
            }catch (e) {
                return res.status(ERRCODE.E_401).send(new MessageError(ERRCODE.E_401, ERRSTR.S_NOTFOUND,e.message))
            }
            toASubOrders[i].orderId = order.id.toString()
            await ToASubOrderController.repo.save(toASubOrders)
        }
        return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, ERRSTR.S_OK, order))
    };

    static deleteOrder = async (req: Request, res: Response) => {
        const id = req.params.orderid
        try {
            await OrderController.repo.findOneOrFail(id)
        } catch (e) {
            return res.status(ERRCODE.E_404).send(e)
        }
        console.log(id)
        await OrderController.repo.delete(id)
        return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, ERRSTR.S_OK))
    }

}
