import {getRepository} from "typeorm";
import {Request, Response} from "express";
import {ERRCODE, ERRSTR, MessageError, Role} from "../helper/helper";
import {validate} from "class-validator";
import {Factory, FactoryType} from "../entity/Factory";
import {User} from "../entity/User";
import {UserController} from "./UserController";
import {SettingsController} from "./SettingsController";
import {Whitelist} from "../entity/Whitelist";

export class FactoryController {
    public static get repo() {
        return getRepository(Factory)
    }

    private static get whitelistRepo() {
        return getRepository(Whitelist)
    }

    static listAll = async (req: Request, res: Response) => {
        let id = JSON.parse(<string>req.headers['userinfo']).user.id
        // let id = res.locals.jwtPayload.userId
        let user: User = null
        let orders: Factory[] = []
        try {
            user = await UserController.repo.findOneOrFail(id)
        } catch (e) {
            return res.status(ERRCODE.E_400).send(new MessageError(ERRCODE.E_400, ERRSTR.S_NOTFOUND))
        }
        try {
            let order: Factory = null
            if (user.role.some(x => x === Role.User)) {
                order = await FactoryController.repo.findOneOrFail(user.companyId)
                orders.push(order)
            } else {
                orders = await FactoryController.repo.find()
            }
        } catch (e) {
            return res.status(ERRCODE.E_401).send(new MessageError(ERRCODE.E_401, ERRSTR.S_NOTFOUND))
        }
        return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, ERRSTR.S_OK, orders))
    }

    static getOneById = async (req: Request, res: Response) => {
        const id = req.params.factoryid
        try {
            const user = await FactoryController.repo.findOneOrFail(id)
            return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, ERRSTR.S_OK, user))
        } catch (e) {
            return res.status(ERRCODE.E_401).send(ERRSTR.S_NOTFOUND)
        }
    }

    static getOneByName = async (req: Request, res: Response) => {
        const factoryname: string = req.params.factoryname
        let factory: Factory = null
        console.log('--------getbyname---------------')
        try {
            factory = await FactoryController.repo.findOneOrFail({name: factoryname})
            return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, ERRSTR.S_OK, factory))
        } catch (e) {
           return res.status(ERRCODE.E_401).send(ERRSTR.S_NOTFOUND)
        }
    }

    static newOrder = async (req: Request, res: Response) => {
        let {type, name, note, contact, toASubOrderIds, factoryWebsite} = req.body
        let user: Factory = new Factory(type, name, note, contact, toASubOrderIds, factoryWebsite)
        try {
            const errors = await validate(user)
            if (errors.length > 0) {
                return res.status(ERRCODE.E_400).send(new MessageError(ERRCODE.E_400, ERRSTR.S_NOTFOUND))
            }
            console.log('xxxx')
            await FactoryController.repo.save(user)
        } catch (e) {
            console.log('hixxx', user, e)
            return res.status(ERRCODE.E_404).send(new MessageError(ERRCODE.E_404, ERRSTR.S_SAVEERR))
        }
        return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, ERRSTR.S_OK, user))
    }

    static editOrder = async (req: Request, res: Response) => {
        let id = req.params.factoryid
        let {type, name, note, contact, toASubOrderIds, factoryWebsite} = req.body
        let userOld: Factory = null
        try {
            userOld = await FactoryController.repo.findOneOrFail(id)
        } catch (e) {
            return res.status(ERRCODE.E_404).send(new MessageError(ERRCODE.E_404, ERRSTR.S_NOTFOUND))
        }
        let user = new Factory(type, name, note, contact, toASubOrderIds, factoryWebsite)
        user.id = userOld.id
        const errors = await validate(user)
        if (errors.length > 0) {
            return res.status(ERRCODE.E_400).send(new MessageError(ERRCODE.E_400, '', errors))
        }
        console.log(user)
        try {
            await FactoryController.repo.save(user)
        } catch (e) {
            return res.status(ERRCODE.E_400).send(new MessageError(ERRCODE.E_400, ERRSTR.S_SAVEERR))
        }
        return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, ERRSTR.S_OK, user))
    }

    static deleteOrder = async (req: Request, res: Response) => {
        const id = req.params.factoryid
        try {
            await FactoryController.repo.findOneOrFail(id)
        } catch (e) {
            return res.status(ERRCODE.E_404).send(e)
        }
        await FactoryController.repo.delete(id)
        return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, ERRSTR.S_OK))
    }

    static listAllCustomers = async (req: Request, res: Response) => {
        let customers, customersInWl;
        try {
            customers = await FactoryController.repo.find({where: {type: 'customer'}})
        } catch (e) {
            return res.status(ERRCODE.E_400).send(new MessageError(ERRCODE.E_400, ERRSTR.S_NOTFOUND))
        }

        return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, ERRSTR.S_OK, customers))
    }

    static listCustomersNotInWL = async (req: Request, res: Response) => {
        let customers, customersInWl;
        try {
            customers = await FactoryController.repo.find({where: {type: 'customer'}})
            const whitelists = await FactoryController.whitelistRepo.find();
            console.log('FactoryController customers-->', customers)
            console.log('FactoryController whitelists-->', whitelists)

            customers = customers.filter(c => {
                return !whitelists.some(wl => wl.companyId === c.id.toString() )
            })
        } catch (e) {
            return res.status(ERRCODE.E_400).send(new MessageError(ERRCODE.E_400, ERRSTR.S_NOTFOUND))
        }

        return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, ERRSTR.S_OK, customers))
    }
}
