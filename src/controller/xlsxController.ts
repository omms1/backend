import {getRepository} from "typeorm";
import {Order} from "../entity/Order";
import {Request, Response} from "express";
import {ERRCODE, ERRSTR, MessageError} from "../helper/helper";
import {validate} from "class-validator";
import * as xlsx from 'xlsx';


const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

export class xlsxController {
    private static get repo() {
        return getRepository(Order)
    }

    static imageUpload = async (req: Request, res: Response) => {
    let userPicture = await req.files[0].filename;
        // const fs = require('fs');
        // fs.unlink(__dirname+ '/test.txt', function (err) {
        //     if (err) {
        //         console.error(err);
        //     }
        //     console.log('File has been Deleted');
        // });
        return res.status(ERRCODE.E_OK).send(JSON.stringify(userPicture))
    };

    static exportSheet = async (req: Request, res: Response) => {
        let data = await xlsxController.repo.find();
        const errors = await validate(data);
        if (errors.length > 0) {
            return res.status(ERRCODE.E_400).send(new MessageError(ERRCODE.E_400, ERRSTR.S_NOTFOUND))
        }
    return res.status(ERRCODE.E_OK).send(data)
        };

    // private static saveAsExcelFile(buffer: any, fileName: string): void {
    //     // let buffer = Buffer.from(arraybuffer);
    //     // let arraybuffer = Uint8Array.from(buffer).buffer;
    //     let Blob = require('blob');
    //
    //     const data: Blob = new Blob([buffer], {
    //         type: EXCEL_TYPE
    //     });
    //     FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    // }
}
