import {getRepository, Between} from "typeorm";
import {Request, Response} from "express";
import {ERRCODE, ERRSTR, MessageError} from "../helper/helper";
import {OrderStatusLog} from "../entity/OrderStatusLog";
import {OrderStatusLogService} from "../service/OrderStatusLogService";

export class OrderStatusLogController {
    private static get repo() {
        return getRepository(OrderStatusLog)
    }

    static listAll = async (req: Request, res: Response) => {
        const users = await OrderStatusLogController.repo.find()
        return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, ERRSTR.S_OK, users))
    }

    static getOneById = async (req: Request, res: Response) => {
        const id = req.params.orderid
        try{
           const user = await OrderStatusLogController.repo.findOneOrFail(id)
            return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, ERRSTR.S_OK, user))
        }catch (e) {
            return res.status(ERRCODE.E_401).send(ERRSTR.S_NOTFOUND)
        }
    }

    static getOrderInfosForSendingEmail = async (req: Request, res: Response) => {
        let {toStatus, hours}  = req.body;
        console.log('toStatus is ',toStatus, 'hours are', hours)
        const orderStatusLogService = new OrderStatusLogService()

        try{
            orderStatusLogService.sendEmailService(toStatus, hours)
        }catch (e) {
            return res.status(ERRCODE.E_404).send(new MessageError(ERRCODE.E_404, ERRSTR.S_SAVEERR))
        }
        return res.status(ERRCODE.E_OK).send(new MessageError(ERRCODE.E_OK, ERRSTR.S_OK))
    }




}
