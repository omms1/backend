import {OrderStatus} from "./Order";

export interface OrderStatusTimeObject {
  orderStatus: OrderStatus;

  datetimeEasternTime: string;

}

export interface EmailContent{

    sender: string;

    receiverEmail: string;

    orderId: String;

    orderNote: String;

    orderStatusTimeObject: OrderStatusTimeObject[];

}
