import {Entity, ObjectIdColumn, ObjectID, Column, CreateDateColumn, UpdateDateColumn, Unique, Index} from "typeorm";
import {IsNotEmpty, Length} from "class-validator";
import * as bcrypt from 'bcryptjs';
import {Role} from "../helper/helper";
import {FactoryType} from "./Factory";

@Entity()
@Unique(['useremail', 'userName'])
export class User {

    @ObjectIdColumn()
    id: ObjectID;

    @Column() //用户名
    // @Length(4,20)
    userName: string;

    @Column()
    userPicture: string;

    @Column() //密码
    @Length(4, 100)
    password: string;

    @Column() //国内手机
    phone1: string;

    @Column() //国外手机
    phone2: string;

    @Index({ unique: true })
    @Column() //电子邮件
    useremail: string;

    @Column() //角色
    @IsNotEmpty()
    role: string[];

    @Column() // 所属公司类型
    companyType: FactoryType

    @Column()//公司ID
    companyId: string;

    @Column() //职位
    duty: string;

    @Column() //创建日期
    @CreateDateColumn()
    createdAt: number;

    @Column() //更新日期
    @UpdateDateColumn()
    updateAt: number;

    @Column() //删除日期
    deleteAt: number;

    @Column() //是否删除
    deleted: boolean;

    @Column() //是否激活
    active: boolean;


    hashPassword() {
        this.password = bcrypt.hashSync(this.password, 8)
    }

    checkIfUnencryptPasswordValid(unEncryptPasswordValid: string) {
        return bcrypt.compareSync(unEncryptPasswordValid, this.password)
    }

    constructor(userName: string, userPicture: string, password: string,  phone1: string, phone2: string, useremail: string, role: Role[], companyType: FactoryType,
                companyId: string, duty: string, createdAt: number, updateAt: number, deleteAt: number, active: boolean = false) {
        this.userPicture = userPicture
        this.userName = userName
        this.password = password
        this.phone1 = phone1
        this.phone2 = phone2
        this.useremail = useremail
        this.role = role
        this.companyType = companyType
        this.companyId = companyId
        this.duty = duty
        this.createdAt = createdAt
        this.updateAt = updateAt
        this.deleteAt = deleteAt
        this.deleted = false
        this.active = active
    }
}

