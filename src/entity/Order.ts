import {Column, Entity, ObjectID, ObjectIdColumn} from "typeorm";

export interface DownPayment { //工厂预付款
    factoryId: string;
    amount: number;
    notes: string;
}

export interface BlancePayment { //工厂尾款
    factoryId: string;
    amount: number;
    notes: string;
}

export enum OrderStatus {
    initial, //初始化订单
    signWithC, //B跟C签订合同
    manufacturing, //工厂生产中
    delivery, //发货
    returnTax, //退税
    done//留存
}


// ## represents the corresponding excel column name
@Entity()
export class Order {
    @ObjectIdColumn()
    id: ObjectID;
    // ************
    // 公共字段
    // ************
    @Column() //备注
    note: string;

    @Column()
    status: OrderStatus

    // ************
    // C与B签订合同阶段
    // ************
    @Column() //发票号
    invoiceNumber: string;

    @Column() //客户C ID
    customerId: string;

    @Column() //工厂 id
    factoryId: string[]

    @Column() //PO# B and C, B给A， 也就是B和C的Invoice number
    poNumber: string;

    @Column() //金额美元
    balanceUsd: number;

    @Column() //金额加元
    balanceCad: number;

    @Column() //航程, ex: 青岛到LA
    flightInformation: string;

    @Column() //柜型
    cabinetType: string;

    @Column() //工厂合同 with manufacturer
    toASubOrderIds: string[];

    @Column() //预计发货时间
    expectedDeliveryDate: number


    //todo need to delete
    @Column()//工厂预付款
    factoryAdvancedPayment: DownPayment[];

    //todo need to delete
    @Column() //工厂尾款
    factoryFinalPayment: BlancePayment[]

    constructor(note: string, status: OrderStatus, invoiceNumber: string, customerId: string, factoryId: string[],
                poNumber: string, balanceUsd: number, balanceCad: number, flightInformation: string,
                cabinetType: string, toASubOrderIds: string[], expectedDeliveryDate: number,
                factoryAdvancedPayment: DownPayment[], factoryFinalPayment: BlancePayment[] ) {
        this.note = note
        this.status = status
        this.invoiceNumber = invoiceNumber
        this.customerId = customerId
        this.factoryId = factoryId
        this.poNumber = poNumber
        this.balanceUsd = balanceUsd
        this.balanceCad = balanceCad
        this.flightInformation = flightInformation
        this.cabinetType = cabinetType
        this.toASubOrderIds = toASubOrderIds
        this.expectedDeliveryDate = expectedDeliveryDate
        this.factoryAdvancedPayment = factoryAdvancedPayment
        this.factoryFinalPayment = factoryFinalPayment
    }


}
