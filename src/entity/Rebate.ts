import {Column} from "typeorm";

export interface Rebate {

    //客户款
    customerPayment: number;

    //货代ID
    agentId: string;

    //货代名称
    agentName: string;

    //货代款金额
    agentBalance: number;

    //货代支付
    agentPayment: number;

    //收到货代发票
    agentInvoice: boolean;

    //报关行名称
    declarationName: string;

    //报关费金额
    declarationFee: number;

    //报关费支付
    declarationPayment: boolean;

    //收到报关费发票
    declarationInvoice: boolean;

    //卡车费
    truckFee: number;

    //卡车费用支付
    truckFeePayment: number;

    //收到卡车费发票
    trackFeeInvoice: boolean;

    //其他发票，收到后费用后打Y
    otherFeeInvoice: boolean;

    //退税申请
    refundApplyCredit: number;

    //申请金额
    actualApplyBalance: number;

    //退税到账
    receiveRefund: boolean;

    //利润分析
    returnAnalyze: string;

    //到账金额
    receivePayment: number;

}
