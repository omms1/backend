import {Column, Entity, Index, ObjectID, ObjectIdColumn} from "typeorm";

export enum FactoryType {
    amanufacturer  = 'manufacturer',
    bcompany = 'myself',
    customer = 'customer'
}

@Entity()
export class Factory {

    @ObjectIdColumn()
    id: ObjectID

    @Column()
    type: FactoryType;

    @Index({ unique: true })
    @Column()
    name: string

    @Column()
    note?: string;

    @Column()
    contact?: string

    @Column()
    toASubOrderIds?: string[]

    @Column()
    factoryWebsite?: string
    constructor(type: FactoryType, name: string, note?: string, contact?: string, toASubOrderIds?: string[], factoryWebsite?: string) {
        this.type = type
        this.name = name
        this.note = note
        this.contact = ''
        this.toASubOrderIds = toASubOrderIds
        this.factoryWebsite = factoryWebsite
    }

}
