import {Column, Entity, ObjectID, ObjectIdColumn} from "typeorm";
import {IsInt, Max, Min} from "class-validator";

export enum MsgType{
    msg=0,
    email=1
}

@Entity()
export class Whitelist {
    @ObjectIdColumn()
    id: ObjectID;

    @Column()
   companyId: string;

    @Column()
    @IsInt()
    @Min(0)
    @Max(1)
    messageType: MsgType;

    @Column()
    createTime: Date;


    @Column()
    operator: string;


    constructor(companyId: string, messageType: 1 | 0, operatorUsername: string) {
        this.companyId = companyId;
        this.messageType = messageType;
        this.operator = operatorUsername;
        this.createTime = new Date();
    }
}
