import {Column, Entity, Index, ObjectID, ObjectIdColumn} from "typeorm";
import {Part} from "./Part";
import {Delivery} from "./Delivery";


@Entity()
export class ToASubOrder {
    // ************
    // 公共字段
    // ************
    @ObjectIdColumn()
    id: ObjectID;

    @Index({ unique: true })
    @Column() //合同号，对方给的,工厂合同 with manufacturer
    contractNum: string;

    @Column() // parent order id
    orderId: string;

    @Column() //合同金额
    contractPriceRmb: number;

    @Column() //downPayment
    downPaymentRmb: number;

    // @Column() // balance, 可以通过contractPrice - downpaymentPrice
    // balanceRmb: number;

    @Column() //备注
    note: string;

    @Column() //航程
    flightInformation: string;

    // ************
    // 生产阶段
    // ************
    //todo change Part to Part[], coz each subOrder can contain several parts
    @Column() //part name
    part: Part[];
    // ************
    // 发货阶段
    // ************
    @Column()
    delivery: Delivery;

    constructor(contractNum: string, orderId: string, contractPriceRmb: number, downPaymentRmb: number,
                note: string, flightInformation: string, part?: Part[], delivery?: Delivery) {
        this.contractNum = contractNum
        this.orderId = orderId
        this.contractPriceRmb = contractPriceRmb
        this.downPaymentRmb = downPaymentRmb
        this.note = note
        this.flightInformation = flightInformation
        this.part = part
        this.delivery = delivery
    }
}

