import {Column, Entity, ObjectID, ObjectIdColumn} from "typeorm";
import {Order, OrderStatus} from "./Order";


export enum isSentEmail {
     notSend = 0,
     sent = 1
}

@Entity()
export class OrderStatusLog {
    @ObjectIdColumn()
    id: ObjectID;

    @Column()
    orderId: string;

    @Column()
    fromStatus: OrderStatus;

    @Column()
    toStatus: OrderStatus;


    @Column()
    changeTime: Date;


    @Column()
    operator: string;

    @Column()
    isSent: isSentEmail;


    constructor(orderId: string, fromStatus: OrderStatus, toStatus: OrderStatus, changeTime: Date, operatorUsername: string) {
        this.orderId = orderId;
        this.fromStatus = fromStatus;
        this.toStatus = toStatus;
        this.changeTime = changeTime;
        this.operator = operatorUsername;
        this.isSent = isSentEmail.notSend;
    }
}
