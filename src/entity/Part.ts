import {Column, Entity, ObjectID, ObjectIdColumn} from "typeorm";


export interface Part {
    blueprintNum: string // 产品图号
    desc: string  //中文描述
    material: string// 材质
    //BDI number
    bdiNumber: string
    //original number
    originalNumber: string
    //数量
    count: number
    //单重
    weight: number
    // @Column() // 总重量，weight * count
    //模具
    needPattern: boolean
    //浇铸	热处理	毛胚	机加	精磨	成品	库存	实际发货
    cast: number
    preHeat: number
    embryo: number
    machineProcess: number
    tune: number
    done: number
    stock: number
    delivered: number
}
