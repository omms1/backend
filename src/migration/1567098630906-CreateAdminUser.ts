import {getRepository, MigrationInterface, QueryRunner} from "typeorm";
import {User} from "../entity/User";
import {Role} from "../helper/helper";
import {Factory, FactoryType} from "../entity/Factory";
import {Order, OrderStatus} from "../entity/Order";
import {ToASubOrder} from "../entity/ToASubOrder";
import {Part} from "../entity/Part";
import {Delivery} from "../entity/Delivery";

export class CreateAdminUser1567098630906 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        // Create Factories

        let factoryArr: Factory[] = [
            new Factory(FactoryType.bcompany, 'BDI Canada', '', '', [], '' ),
            // new Factory(FactoryType.customer, 'ESSO', '','', [], ''),
            // new Factory(FactoryType.customer, 'PetroCanada', '', '', [], ''),
            // new Factory(FactoryType.customer, 'Toyota', '', '', [], ''),
            // new Factory(FactoryType.customer, 'Honda', ''),
            // new Factory(FactoryType.customer, 'Shell', ''),
            // new Factory(FactoryType.customer, 'BMW', ''),
            // new Factory(FactoryType.customer, 'Food', ''),
            // new Factory(FactoryType.customer, 'FreshCo', ''),
            // new Factory(FactoryType.customer, 'Walmart', ''),
            // new Factory(FactoryType.customer, 'Costco', ''),
            // new Factory(FactoryType.customer, 'Ecco', ''),
            // new Factory(FactoryType.customer, 'BMO', ''),
            // new Factory(FactoryType.customer, 'RBC', ''),
            // new Factory(FactoryType.customer, 'TD', ''),
            // new Factory(FactoryType.customer, 'CIBC', ''),
            // new Factory(FactoryType.customer, 'Scotia', ''),
            // new Factory(FactoryType.customer, 'Telus', ''),
            // new Factory(FactoryType.customer, 'Rogers', ''),
            // new Factory(FactoryType.customer, 'Bell', ''),
            // new Factory(FactoryType.customer, 'Koodo', ''),
            // new Factory(FactoryType.customer, 'Air Canada', ''),
            // new Factory(FactoryType.customer, 'West jet', ''),
            // new Factory(FactoryType.customer, 'Welcome center', ''),
            // new Factory(FactoryType.customer, 'Good life fitness', ''),
            // new Factory(FactoryType.customer, 'Good fitness', ''),
            // new Factory(FactoryType.customer, 'Bodyview fitness', ''),
            // new Factory(FactoryType.customer, 'Snap fitness', ''),
            // new Factory(FactoryType.customer, 'Seneca', ''),
            // new Factory(FactoryType.customer, 'HomeDopt', ''),
            // new Factory(FactoryType.customer, 'Centennial', ''),
            // new Factory(FactoryType.amanufacturer, 'HEIER', ''),
            // new Factory(FactoryType.amanufacturer, 'HAIXIN', ''),
            // new Factory(FactoryType.amanufacturer, 'HAWEI', ''),
            // new Factory(FactoryType.amanufacturer, 'ZHONGXING', ''),
            // new Factory(FactoryType.amanufacturer, 'YIDONG', ''),
            // new Factory(FactoryType.amanufacturer, 'LIANTONG', ''),
            // new Factory(FactoryType.amanufacturer, 'DIANXIN', ''),
            // new Factory(FactoryType.amanufacturer, 'GONGHANG', ''),
            // new Factory(FactoryType.amanufacturer, 'ZHONGHANG', ''),
            // new Factory(FactoryType.amanufacturer, 'JIANHANG', ''),
            // new Factory(FactoryType.amanufacturer, 'NONGHANG', ''),
            // new Factory(FactoryType.amanufacturer, 'JIAOHANG', ''),
        ]


        let factoryRepo = getRepository(Factory)
        for (let i = 0; i < factoryArr.length; i++) {
            await factoryRepo.save(factoryArr[i])
        }

        let userArr: User[] = [
            new User('mark2win', '', 'mark2win', '33344', '4433',
                'mark@mark2win.com', [Role.Admin],FactoryType.bcompany, factoryArr[0].id.toString(),
                'representative', Date.now(), null, null, true),
            // new User('hondaUser', 'admin1', '33344', '4433',
            //     '3264759@qq.com', [Role.User], FactoryType.customer, factoryArr[1].id.toString(),
            //     'representative', Date.now(), null, null, true),
            // new User('toyotaUser', 'admin2', '33344', '4433',
            //     'john.fujun.ca@outlook.com', [Role.User], FactoryType.customer, factoryArr[2].id.toString(),
            //     'representative', Date.now(), null, null, true),
            // new User('bmwUser', 'admin3', '33344', '4433',
            //     'markca2001@hotmail.com', [Role.User], FactoryType.customer, factoryArr[3].id.toString(),
            //     'representative', Date.now(), null, null, true),
            // new User('essoUser', 'admin3', '33344', '4433',
            //     '333@333.com', [Role.User], FactoryType.customer, factoryArr[3].id.toString(),
            //     'representative', Date.now(), null, null, true),
            // new User('shellUser', 'admin3', '33344', '4433',
            //     '444@444.com', [Role.User], FactoryType.customer, factoryArr[3].id.toString(),
            //     'representative', Date.now(), null, null, true),
            // new User('tdUser', 'admin3', '33344', '4433',
            //     '533@333.com', [Role.User], FactoryType.customer, factoryArr[3].id.toString(),
            //     'representative', Date.now(), null, null, true),
            // new User('haierUser', 'admin3', '33344', '4433',
            //     '633@333.com', [Role.User], FactoryType.amanufacturer, factoryArr[3].id.toString(),
            //     'representative', Date.now(), null, null, true),
            // new User('haixinUser', 'admin3', '33344', '4433',
            //     '733@333.com', [Role.User], FactoryType.amanufacturer, factoryArr[3].id.toString(),
            //     'representative', Date.now(), null, null, true),
            // new User('zhongxingUser', 'admin3', '33344', '4433',
            //     '833@333.com', [Role.User], FactoryType.amanufacturer, factoryArr[3].id.toString(),
            //     'representative', Date.now(), null, null, true),
        ]
        let repoUser = getRepository(User)
        for (let i = 0; i < userArr.length; i++) {
            userArr[i].hashPassword()
            await repoUser.save(userArr[i])
        }

        // let orderArr: Order[] = [
        //     new Order('', OrderStatus.manufacturing, '123', factoryArr[1].id.toString(),
        //         [factoryArr[3].id.toString()], '789', 10000, 13000,
        //         '', '', [''], 124,
        //         [{factoryId: factoryArr[2].id.toString(), amount: 1000, notes: ''}],
        //         [{factoryId: factoryArr[2].id.toString(), amount: 9000, notes: ''}]),
        //     new Order('', OrderStatus.delivery, '111', factoryArr[2].id.toString(),
        //         [factoryArr[4].id.toString()], '333', 10000, 13000,
        //         '', '', [''], 444,
        //         [{factoryId: factoryArr[3].id.toString(), amount: 1000, notes: ''}],
        //         [{factoryId: factoryArr[3].id.toString(), amount: 8000, notes: ''}]),
        // ]
        // let orderRepo = getRepository(Order)
        // for (let i = 0; i < orderArr.length; i++) {
        //     await orderRepo.save(orderArr[i])
        // }

        let partArr: Part[] = [
            {blueprintNum: '', desc: '', material: '', bdiNumber: '', originalNumber: '', count: 1000,
                weight: 1000, needPattern: false, cast: 111, preHeat: 111, embryo: 111, machineProcess: 111,
                tune: 111, done: 111, stock: 111, delivered: 111},
            {blueprintNum: '', desc: '', material: '', bdiNumber: '', originalNumber: '', count: 1000,
                weight: 1000, needPattern: false, cast: 111, preHeat: 111, embryo: 111, machineProcess: 111,
                tune: 111, done: 111, stock: 111, delivered: 111}
        ]

        let deliveryArr: Delivery[] = [
            {handOverTime: 111, cabinetType: '', billOfLadingName: '', scheduledDeliveryTime: 111,
                factoryDeliveryNotice: false, factoryIntoTheGoods: false, sendIsf: false, containerYardCloseTime: 111,
            boardingDate: 111, confirmReceiveBillOfLading: false, sendPauline: false,
                confirmReceiveDeclaration: false, nanaReceive: false, sendInvoiceToFactory: false,
            factoryInvoiceBalance: 111, receiveInvoice: false, declarationMatch: false},
            {handOverTime: 222, cabinetType: '', billOfLadingName: '', scheduledDeliveryTime: 222,
                factoryDeliveryNotice: false, factoryIntoTheGoods: false, sendIsf: false, containerYardCloseTime: 222,
                boardingDate: 222, confirmReceiveBillOfLading: false, sendPauline: false,
                confirmReceiveDeclaration: false, nanaReceive: false, sendInvoiceToFactory: false,
                factoryInvoiceBalance: 222, receiveInvoice: false, declarationMatch: false}
        ]

        // let toASubOrderArr: ToASubOrder[] = [
        //     new ToASubOrder('1', '', 10000, 5000,
        //         '', '', [partArr[0]], deliveryArr[0]),
        //     // new ToASubOrder('2', orderArr[1].id.toString(), 10000, 5000,
        //     //     '', '', [partArr[1]], deliveryArr[1])
        // ]
        //
        // let toASubOrderRepo = await getRepository(ToASubOrder);
        // let toASubOrderIdArr: string[] = [];
        // for (let i = 0; i< toASubOrderArr.length; i++) {
        //     let res = await toASubOrderRepo.save(toASubOrderArr[i]);
        //     toASubOrderArr[i].id = res.id;
        //     toASubOrderIdArr.push(res.id.toString())
        // }
        // let factory1Repo = getRepository(Factory)
        // for (let i = 0; i < factoryArr.length; i++) {
        //     factoryArr[i].toASubOrderIds = toASubOrderIdArr
        //     await factory1Repo.save(factoryArr[i])
        // }

    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
