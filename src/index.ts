 "use strict";
import "reflect-metadata";
import {createConnection} from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";

import * as cors from 'cors';
import * as helmet from "helmet";
import routes from "./routes";

const HOSTPORT = process.env.PORT || 3838;
// const ENV = process.env.ENV || "development";

createConnection().then(async connection => {
    const app = express();
    // app.use(function(req, res, next) {
    //     res.header("Access-Control-Allow-Origin", "*");
    //     res.header(
    //         "Access-Control-Allow-Headers",
    //         "Origin, X-Requested-With, Content-Type, Accept"
    //     );
    //     res.send('HEY here11');
    //     next();
    // });
    // app.use(cors())
    console.log("tetssetseteststestestsetestsetlkesjlksjglkrgjlkrjglkrsgnerlkgnlrgnlkerglknern")
    app.use(helmet())
    app.use(bodyParser.json());
    app.use("/", routes)
    app.listen(HOSTPORT);
    console.log(`Express server has started on port ${HOSTPORT}`, `Open http://localhost:3838/users to see results`);

}).catch(error => console.log(error));

