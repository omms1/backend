import {NextFunction, Request, Response} from "express";
import {User} from "../entity/User";
import {getRepository} from "typeorm";
import {ERRCODE, ERRSTR, MessageError} from "../helper/helper";

export const checkRole = (roles: Array<string>) => {
    // console.log('first ->', roles)
    roles = roles.map(r => r.toLowerCase())
    // console.log('2nd->', roles)
    return async (req: Request, res: Response, next: NextFunction) => {
        let id = res.locals.jwtPayload.userId
        let user: User = null
        try {
            user = await getRepository(User).findOneOrFail(id)
        }catch (e) {
            return res.status(ERRCODE.E_404).send(new MessageError(ERRCODE.E_404, ERRSTR.S_NOTFOUND))
        }
        if (user.role.some(r => roles.indexOf(r.toLowerCase()) > -1)) {
            next()
        }else {
            return res.status(ERRCODE.E_401).send(new MessageError(ERRCODE.E_401, ERRSTR.S_BADPARAMETER))
        }
    }
}
