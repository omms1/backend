import  *  as  multer  from  "multer" ;

export const upload = {
     storage: multer.diskStorage({
        destination: (req, file, cb) => {
            console.log(__dirname,"what's directoryname?")
            cb(null, '../omms-frontend/src/assets/images');
//            cb(null, 'http://159.203.40.84/assets/images');
       },
        filename: (req, file, cb) => {
            const ext = file.mimetype.split('/')[1];
            cb(null, file.fieldname + '-avatar' + '.' + ext);
//        cb(null, file.originalname.split(".")[0] + '-' + Date.now() + '.' + ext);
        }
    }),
    fileFilter: function (req, file, cb) {
        if (!file) {
            cb();
        }
        let fileExts = ['png', 'jpg', 'jpeg', 'gif']
        let isAllowedExt = fileExts.includes(file.originalname.split('.')[1].toLowerCase());
        const image = file.mimetype.startsWith('image/');
        if (isAllowedExt && image) {
            cb(null, true);
        } else {
            cb(new Error('Error: File type not allowed!'));
        }
    },
    limits:{
        fileSize: 1024 * 512
    }
};
