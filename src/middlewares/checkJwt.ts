import {NextFunction, Request, Response} from "express";
import * as jwt from "jsonwebtoken"
import config from "../config/config";
import {ERRCODE, MessageError} from "../helper/helper";

export const checkJwt = (req: Request, res: Response, next: NextFunction) => {
    const token = <string>req.headers['authorization']
    let jwtPayload = null
    try {
        jwtPayload = <any>jwt.verify(token, config.jwtSecret)
        res.locals.jwtPayload = jwtPayload
       // app.locals
    }catch (e) {
        return res.status(ERRCODE.E_401).send(new MessageError(ERRCODE.E_404, 'checkJwt'))
    }
    const {userId, useremail, role} = jwtPayload
    const newToken = jwt.sign({userId, useremail, role},
        config.jwtSecret,
        {'expiresIn': '1h'}
        )

    res.setHeader('token', newToken)
    next()
}

