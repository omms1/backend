import {Between, getRepository, MoreThan, ObjectID, Raw} from "typeorm";
import {isSentEmail, OrderStatusLog} from "../entity/OrderStatusLog";
import {validate} from "class-validator";
import {Order, OrderStatus} from "../entity/Order";
import {User} from "../entity/User";
import {EmailService} from "./EmailService";
import config from "../config/config";
import {EmailContent} from "../entity/EmailContent";
import {Factory} from "../entity/Factory";
import {WhitelistService} from "./WhitelistService";
import {Whitelist} from "../entity/Whitelist";


export class OrderStatusLogService{

    private static get repo() {
        return getRepository(OrderStatusLog)
    }

    private static get orderRepo() {
        return getRepository(Order)
    }

    private static get userRepo() {
        return getRepository(User)
    }
    private static get factoryRepo() {
        return getRepository(Factory)
    }

    constructor(){

    }

    createOrderStatusLog = async (orderId: string, fromStatus: OrderStatus, toStatus: OrderStatus, changeTime: Date, operatorUsername: string) => {
        let orderStatusLog: OrderStatusLog = new OrderStatusLog(orderId, fromStatus, toStatus,  new Date(), operatorUsername)
        try{
            const errors = await validate(orderStatusLog)
            if (errors.length > 0) {
                return false;
            }
            await OrderStatusLogService.repo.save(orderStatusLog);
        }catch (e) {
            return false;
        }
        return true;
    }

    sendEmailService = async (toStatus: OrderStatus, hours: number) =>{
        const toDatetime =  Date.now();
        const fromDatetime = toDatetime - 3600000*hours;

        const toDateTimeStr = new Date(toDatetime).toISOString();
        const fromDateTimeStr = new Date(fromDatetime).toISOString();

        const whitelistService = new WhitelistService();
        let whitelists: Whitelist[];
        try{

            whitelists = await whitelistService.queryAllWhitelist();
        }catch(error)
        {
            console.log('Can not fine whitelist', error);
        }


        console.log('fromDatetime',fromDateTimeStr,'toDatetime',toDateTimeStr);
        try{
            const orderStatusLogs = await OrderStatusLogService.repo.find({toStatus, isSent: isSentEmail.notSend })
            //const orderStatusLogs = await OrderStatusLogService.repo.find({changeTime:Between('2019-09-10T02:25:06.779+00:00', '2019-09-12T02:25:06.779+00:00') })
            //const orderStatusLogs = await OrderStatusLogService.repo.find({changeTime:Raw(alias=>`${alias} < NOW()`) })
            console.log('orderStatusLogs =====>', {...orderStatusLogs})
            orderStatusLogs.filter(orderStatusLog => {return new Date(orderStatusLog.changeTime) > new Date(fromDatetime)});

            console.log('orderStatusLogs ---->', {...orderStatusLogs})
            if(orderStatusLogs.length >0 )
            {
                orderStatusLogs.forEach(async(orderStatusLog, index)=>{

                    try{
                        //const order = await OrderStatusLogService.orderRepo.findOneOrFail({where:{_id:'5d7838e4c56d0c61bc9bc7d6'}})
                         const order = await OrderStatusLogService.orderRepo.findOne(orderStatusLog.orderId)
                         const orderStatuses = await OrderStatusLogService.repo.find({where:{orderId: order.id.toString()}});
                        console.log('order ---->', order)
                        if(order)
                        {
                                const factory = await OrderStatusLogService.factoryRepo.findOne(order.customerId)
                                if(!whitelists.some( wl => wl.companyId === factory.id.toString())){
                                    console.log('factory ', factory.name, ' is not in whitelist');
                                    return;
                                }
                                const users = await OrderStatusLogService.userRepo.find({where:{id: factory.contact}})
                                console.log('users are ', users);
                                let myFormattedDateTime = orderStatusLog.changeTime.toLocaleDateString() +'  '+ orderStatusLog.changeTime.toLocaleTimeString();
                                console.log('myFormattedDateTime-->',myFormattedDateTime)
                                let orderStatusTimeObject = [];
                                orderStatuses.forEach((item, index) => {
                                    orderStatusTimeObject.push({
                                        orderStatus: item.toStatus,
                                        datetimeEasternTime: item.changeTime.toLocaleDateString() +'  '+ item.changeTime.toLocaleTimeString()
                                    });
                                })
                                users.forEach( async user => {
                                    const emailContent: EmailContent = {
                                        sender: config.emailService.auth.user,
                                        receiverEmail: user.useremail,
                                        orderId: order.id.toString(),
                                        orderNote: order.note,
                                        orderStatusTimeObject: orderStatusTimeObject
                                    }
                                    const result = await EmailService.sendNotifyEmail(emailContent);
                                })
                                orderStatusLog.isSent = isSentEmail.sent;
                                await OrderStatusLogService.repo.save(orderStatusLog)
                        }
                    }
                    catch (error) {
                        console.log('find order error! Error is ', error)
                        return
                    }
                })
            }
        }catch (e) {
            console.log('e---->', e);
            return
        }



    }

}
