import {getRepository} from "typeorm";
import {Whitelist} from "../entity/Whitelist";

export class WhitelistService{

    private static get repo() {
        return getRepository(Whitelist)
    }

    constructor(){

    }

    public queryAllWhitelist = async () => {
        let wls: Whitelist[];
        try {
            wls = await WhitelistService.repo.find();
         }
         catch(e)
         {
             console.log('queryAllWhitelist is error', e);
             return
         }finally {
            return wls;
        }

    }



}
