import config from "../config/config";
import {EmailContent} from "../entity/EmailContent";
import {OrderStatus} from "../entity/Order";

const nodemailer = require('nodemailer'),
Email = require('email-templates');

export class EmailService{

    static sendNotifyEmail = async (emailContent: EmailContent)=>{

        let htmlContents = [];
        emailContent.orderStatusTimeObject.forEach((item, index)=>{
            switch (item.orderStatus) {
                case OrderStatus.signWithC: {
                    htmlContents.push(`<li><strong>Signed a contract.(${item.datetimeEasternTime})<strong> </li>`);
                    break;
                }
                case OrderStatus.manufacturing: {
                    htmlContents.push(`<li><strong>Manufacturing.(${item.datetimeEasternTime})<strong> </li>`);
                    break;
                }
                case OrderStatus.delivery: {
                    htmlContents.push(`<li><strong>Deliveryed.(${item.datetimeEasternTime})<strong> </li>`);
                    break;
                }
                default:
                    break;
            }
        })

        const processedSteps = htmlContents.length;
        if(1 ==processedSteps){
            htmlContents.push('<li>Manufacturing.</li>');
            htmlContents.push('<li>Deliveryed.</li>');
            htmlContents.push('<li>Received.</li>');
        }
        if(2 ==processedSteps){
            htmlContents.push('<li>Deliveryed.</li>');
            htmlContents.push('<li>Received.</li>');
        }
        if(3 ==processedSteps){
            htmlContents.push('<li>Received.</li>');
        }

        htmlContents.reverse();
        let htmlContentsString ='';
        for (let i = 0; i < htmlContents.length; i++) {
            htmlContentsString = htmlContentsString + htmlContents[i]
        }


        console.log('htmlContents --->', htmlContentsString)
        let mailOptions = {
            to: emailContent.receiverEmail,
            from: emailContent.sender,
            subject: `Status of ${emailContent.orderNote} is changed`,
            html: `<ul>
                      ${htmlContentsString}
                   </ul>`
        };


        let transporter =  nodemailer.createTransport(
            {
                host: config.emailService.host,
                port: config.emailService.port,
                secure: false,
                auth:{
                    user: config.emailService.auth.user,
                    pass: config.emailService.auth.pass
                }
            }
        );
         let info = await transporter.sendMail(mailOptions, function (err, info1) {
             if(err){
                 console.log('err--->',err);
             }
             else
             {
                 console.log('info1--->',info1);
             }
             return
         });
        console.log('Message sent successfully!');
        console.log(nodemailer.getTestMessageUrl(info));
        //transporter.close();
    }


}
