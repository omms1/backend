module.exports = {
  apps : [{
    name: 'OMMS_API',
    script: 'build/index.js',

    // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
    args: 'one two',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }],

  deploy : {
    production : {
      user : 'ubuntu',
      host : 'ec2-35-175-202-170.compute-1.amazonaws.com',
      ref  : 'origin/OMMS-32-bugfix',
      key: '~/.ssh/OMMS.pem',
      repo : 'git@gitlab.com:omms-project/omms-backend.git',
      path : '/home/ubuntu/production',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
    }
  }
};
